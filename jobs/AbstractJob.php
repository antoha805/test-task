<?php

namespace jobs;

abstract class AbstractJob
{
    const RESULT_SUCCESS = 0;
    const RESULT_FAIL = 1;

    protected int $tries = 1;
    protected string $alias;

    abstract function handle();

    final public function getAlias(): string
    {
        return $this->alias ?? get_class($this);
    }

    final public function getTries(): int
    {
        return $this->tries;
    }
}