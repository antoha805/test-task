<?php

namespace jobs;

class Job2 extends AbstractJob
{
    private ?array $someData;
    protected string $alias = 'job2';

    public function __construct(array $someData = null)
    {
        $this->someData = $someData;
    }

    public function handle()
    {
        // do something with $someData
    }
}