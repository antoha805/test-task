I am inspired by the Laravel solution (https://laravel.com/docs/7.x/scheduling, https://laravel.com/docs/7.x/queues).
So I tried to implement something similar (of course much simplified).

The logic of realization:
Cron should be set up to call schedule.php file script every minute. See the crontab example file.
schedule.php file script just runs JobService.php schedule method
JobService.php schedule method has a list of tasks (jobs) and calls them at a concrete time or with a concrete period (see 'at' and 'cron' methods)
Every job is an instance that extends AbstractJob abstract class. The job object has a 'tries' field (means a maximum amount of fail calls) and 'handle' methods with some logic. JobService runs the job`s logic and stores the result of every trying to database (job_results table).
 
The code has a constant list of jobs and times when they should be called. But if we need to change this in runtime, the logic of    
 JobService.php's schedule method should be changed. For example, we can store jobs list and times when they should be called to a database (jobs table) and JobService.php's schedule method will use this information.