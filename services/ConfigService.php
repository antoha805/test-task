<?php

namespace services;

class ConfigService
{
    public function getDBSettings()
    {
        return [
            'host' => 'localhost',
            'port' => 3306,
            'database' => 'demo',
            'user' => 'root',
            'password' => ''
        ];
    }
}