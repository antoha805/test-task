<?php

namespace services;

use jobs\AbstractJob;
use jobs\Job1;
use jobs\Job2;
use jobs\Job3;

class JobService
{
    public function schedule()
    {
        $this->cron('*/5 * * * *', new Job1());
        $this->cron('0 17 * * 2', new Job2());
        $this->at(1609459200, new Job3());
    }

    private function cron(string $expression, AbstractJob $job)
    {
        $closestTimestamp = $this->getClosestTimestampByCronExtension($expression);

        $currentTimestamp = time();

        if ($currentTimestamp >= $closestTimestamp && $currentTimestamp - $closestTimestamp < 60) {
            $this->handleJob($job);
        }
    }

    private function at(int $timestamp, AbstractJob $job)
    {
        $currentTimestamp = time();

        if ($currentTimestamp >= $timestamp && $currentTimestamp - $timestamp < 60) {
            $this->handleJob($job);
        }
    }

    private function handleJob(AbstractJob $job)
    {
        $logService = new LogService();

        for ($i = 0; $i < $job->getTries(); $i++)
        {
            try {
                $job->handle();
                $logService->logJobHandleSuccess($job);
                return true;
            }
            catch (\Exception $e){
                $logService->logJobHandleFail($job);
            }
        }
        return false;
    }

    private function getClosestTimestampByCronExtension(string $extension): int
    {
        // to calculate closest timestamp could be used library like https://github.com/mtdowling/cron-expression

        $timestamp = 1;

        return $timestamp;
    }
}
