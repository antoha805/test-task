<?php

namespace services;

use jobs\AbstractJob;

class LogService
{
    public function logJobHandleSuccess(AbstractJob $job)
    {
        mysqli_query(
            $this->getDBConnection(),
            "insert into job_results (job, result, time) 
                values (
                    . '" . $job->getAlias() . "', "
            . AbstractJob::RESULT_SUCCESS . ', '
            . time()
            . ')'
        );
    }

    public function logJobHandleFail(AbstractJob $job)
    {
        mysqli_query(
            $this->getDBConnection(),
            "insert into job_results (job, result, time) 
                values (
                    . '" . $job->getAlias() . "', "
            . AbstractJob::RESULT_FAIL . ', '
            . time()
            . ')'
        );
    }

    private function getDBConnection()
    {
        $dbSettings = (new ConfigService())->getDBSettings();

        $connection = mysqli_connect(
            $dbSettings['host'],
            $dbSettings['user'],
            $dbSettings['password'],
            $dbSettings['database'],
            $dbSettings['port'],
        );

        if (!$connection){
            throw new \Exception('DB connection error');
        }
        return $connection;
    }
}
